---
layout: job_page
title: "Senior Sales Development Representative"
---

As a Senior Sales Development Representative (SDR) you will develop interest in GitLab among the largest accounts served by our Strategic Account Leaders. In addittion, it is expected that you will lead the SDR team from the front in every way that the team is measured. You will be a source of knowledge and best practices amongst the outbound SDRs, and will help to train, onboard, and mentor new SDRs.

## Responsibilities

* Meet or exceed Senior Sales Development Representative (SDR) sourced Sales Accepted Opportunity (SAO) volume targets.
* Act as a mentor for new SDR hires in helping them navigate their key accounts.
* Review/work accounts in the strategic segment in alignment with GitLab's go-to-market priorities. 
* Work closely with the Sales and Business Development Manager as well as the Regional Sales Directors (RDs) to identify key company accounts to develop.
* Work in collaboration with Online Marketing to develop targeted marketing tactics against your assigned target accounts.
* Work in collaboration with Field Marketing to deliver joint efforts to develop your assigned target accounts.
* Work in collaboration with Content and Product Marketing to develop effective messaging for outbound communications to your assigned accounts.

## Hiring Process

Additional details about our process can be found on our [hiring page](/handbook/hiring).

**Note on Compensation**

As you use the compensation calculator below, please bear in mind that for this
role the calculated compensation represents your On Target Earnings (OTE). You
will typically get 60% as base, and 40% as bonus based on monthly number of Sales
Accepted Opportunities (SAOs) generated. Also see the [Sales Compensation Plan](https://about.gitlab.com/handbook/finance/sales-comp-plan/).