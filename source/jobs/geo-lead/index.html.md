---
layout: job_page
title: "Geo Lead"
---

[GitLab Geo](https://docs.gitlab.com/ee/gitlab-geo/README.html) is an
enterprise product feature that provides a disaster recovery and adds
redundancy for GitLab instances. As the Geo Lead, you will take overall
responsibility for delivering reliability, performance, and recovery
improvements to make the product ready for mission critical tasks. This
position will report to the VP of Engineering.

## Responsibilities

- Build and lead the team of Geo engineers by actively seeking and hiring globally-distributed talent
- Architect and prioritize Geo and Disaster Recovery products for GitLab
- Lead team meetings, conduct 1:1's, and continuously improve the onboarding and training process/experience.
- Identify ways to load test and improve failover, recovery, and availability of GitLab Geo
- Ensure all GitLab data (e.g. repository data, container registry images, CI build artifacts, etc.) is replicated properly
- Instrument and monitor the health of distributed GitLab instances
- Educate all team members on best practices relating to high availability

## Requirements

- 5 years or more experience in a leadership role
- Experience architecting and implementing fault-tolerant, distributed systems
- In-depth experience with Ruby on Rails, Go, and/or Git
- Excellent spoken and written English
- You share our [values](/handbook/values), and work in accordance with those values
- [A technical interview](/jobs/#technical-interview) is part of the hiring process for this position.
- A customer scenario interview is part of the hiring process for this position.


**NOTE** In the compensation calculator below, fill in "Lead" in the `Level` field for this role.
