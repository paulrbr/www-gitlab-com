---
layout: job_page
title: "Backend Developer, CI/CD Pipelines"
---

CI/CD Backend Developers are primarily tasked with improving the Continuous Integration (CI)
and Continuous Deployment (CD) functionality in GitLab.
GitLab CI/CD is widely used by a number of organizations over the world.
CI/CD engineers are expected to be self-directed, communicative, and versatile;
they should have experience with different developer technologies and frameworks.
Engineers working in that position should be willing to learn Kubernetes and Container Technology.
Engineers at that position should always have three goals in mind:
1. Provide value to the user and communicate such with product managers,
2. Introduce features that work at scale and in untrusting environments,
3. Always focus on defining and shipping [the Minimal Viable Change](https://about.gitlab.com/handbook/product/#the-minimally-viable-change).

We, as a team, cover end-to-end integration of CI/CD in GitLab, with components being written in Rails and Go.
We work on a scale of processing almost two millions of CI/CD jobs on GitLab.com monthly.
CI/CD engineering is interlaced with a number of teams across a GitLab.
We build new features by following our [direction](https://about.gitlab.com/direction/#ci--cd).
Currently, we focus on providing a deep integration of Kubernetes with GitLab:
1. by automating application testing and deployment through Auto DevOps,
2. by managing GitLab Runners on top of Kubernetes,
3. by working with other teams that provide facilities to monitor all running applications,
4. in the future implement A-B testing, feature flags, etc.

Additionally, we also focus on improving the efficiency, performance, and scalability of all aspects of CI/CD:
1. Improve performance of developer workflows, e.g. faster CI testing, by improving parallelization,
2. Improve performance of implementation, ex.: by allowing us to run 10-100x more in one year,
3. Identify and add features needed by us, ex.: to allow us to test more reliable and ship faster.

Being part of the CI/CD team, you have a unique opportunity to use bleeding edge technologies,
such as Kubernetes and Docker, and work on Ruby and Go projects.
Not only will you work on features that deliver value for hundreds of thousands of organizations
around the world, but you will also face massive scale issues with running a CI/CD system.

See the description of the [CI/CD team](/handbook/backend#cicd) for more details.
CI/CD Backend Developers report to the [CI/CD Lead](/jobs/ci-cd-lead/).

## Responsibilities

* Develop CI/CD features from proposal to polished end result,
  by being able to reason about the features with Product Managers,
  and be able to work freely in Rails or Go,
* Support and collaborate with our [Support Engineers](/jobs/support-engineer)
  in getting to the bottom of user-reported issues and come up with robust solutions.
* Manage and review code contributed by the rest of the community and work with them to get it ready for production.
* Create and maintain documentation around features and configuration to save our users time.
* Take initiative in improving the software in small or large ways to address pain points
  in your own experience as a developer.
* Keep code easy to maintain and easy for others to contribute to.

## CI/CD Backend Developers

* Have production-level Rails experience, or Go-experience when working on Kubernetes,
* You can reason about software, algorithms, and performance from a high level,
* You are passionate about open source,
* You are passionate about developer tool space and about the developer needs,
* You have a good understanding of different products and can reason about features being proposed,
* You have strong written communication skills,
* You are self-motivated and have strong organizational skills,
* You share our [values](/handbook/values), and work in accordance with those values.
* [A technical interview](/handbook/hiring/technical) is part of the hiring process for this position.

## CI/CD Senior Backend Developers

* Engineering Skills
    - Write modular, well-tested, and maintainable code
    - Know a domain really well and radiate that knowledge
    - Contribute to one or more complementary projects
    - Take on difficult issues, big or small
    - Be able to reason about the cost of implementing and maintaining the features

* Leadership
    - Begin to show architectural perspective
    - Propose new ideas, performing feasibility analyses and scoping the work
    - Help hire and train new CI/CD Backend Developers
    - Become a go-to person for the other Developers when they face tough challenges
    - Have a good understanding of different workflows, and technologies being used

* Code quality
    - Leave code in substantially better shape than before
    - Fix bugs/regressions quickly
    - Monitor overall code quality/build failures
    - Leave tests in better shape than before: faster, more reliable, more comprehensive, etc.

* Communication
    - Provide thorough and timely code feedback for peers
    - Able to communicate clearly on technical topics and present ideas to the rest of the team
    - Keep issues up-to-date with progress
    - Help guide other merge requests to completion

* Performance and scalability
    - Excellent at writing production-ready code with little assistance
    - Able to write complex code that can scale with a significant number of users
    - Able to fix performance issues on GitLab.com with minimal guidance using our existing tools, and improve those tools where needed

## CI/CD Staff Backend Developers

* Engineering Skills:

    - Proactively identifies and reduces technical debt,
    - Identifies and reduces scalability issues and performance bottlenecks
      in the GitLab stack,
    - Can demonstrate a deep knowledge and experience with multiple technical domains
      as well as a very good broad knowledge of the entire GitLab technology stack.
      What Kent Beck refers to as a
      [Paint Drip Person](https://www.facebook.com/notes/kent-beck/paint-drip-people/1226700000696195)
    - Be able to reason about the cost of implementing and maintaining the features, for example, to be able
      to identify areas of improvement that reduce the running cost of the solutions

* Leadership

    - Have a good understanding of different products and can reason about features being proposed and shipped
    - Actively contributes to the long-term strategic architectural vision for
      the company, helping to set the technical direction for GitLab
    - Works across functional groups, engaging stakeholders, gatekeepers, and
      engineers to deliver major features from idea to production
    - Demonstrates the ability to break a complex problem down into a set of
      [minimum viable changes](http://conversationaldevelopment.com/shorten-cycle/)
      that can be shipped through a series of short release cycles

* Communication

    - Proactively shares knowledge and radiates GitLab technical skills to team
      members through mentorship, documentation, pairing and other approaches
    - Excellent written and spoken language skills: a staff developer should be
      able to communicate their ideas
      [efficiently](https://about.gitlab.com/handbook/values/#efficiency) and concisely

### Engineering experience for all levels

For this position, a significant amount of experience with Ruby is a **strict requirement**.
Extra bonus points is Go experience. We do expect that you will like to work on Go during your journey at GitLab.

If you are great Go developer with a lot of Kubernetes production experience
and the strong drive towards improving a developers workflow please drop us the note. 

We would love to hire all great backend developers, regardless of the language they have most experience with,
but at this point, we are looking for developers who can get up and running within the GitLab code base very quickly
and without requiring much training, which limits us to developers with a large amount of existing experience with Ruby,
and preferably Rails too.

If you think you would be an asset to our engineering team regardless,
please see if [another position](/jobs) better fits your experiences and interests.

If you would still prefer to join the backend development team as a Ruby or Go developer,
please consider contributing to the open-source [GitLab Community Edition](https://gitlab.com/gitlab-org/gitlab-ce)
or [GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner). 
We frequently hire people from the community who have shown, through contributions, that
they have the skills that we are looking for, even if they didn’t have much previous experience
with those technologies, and we would gladly review those contributions.

## Relevant links

- [Engineering Handbook](/handbook/engineering)
- [Engineering Workflow](/handbook/engineering/workflow)

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).
A [technical interview](/handbook/hiring/technical) is part of the process

* Selected candidates will be invited to schedule a 30min [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a 1 hour interview with a CI/CD Engineer or Lead
* Next, depending on your level of experience:
  * Intermediate-level will then be invited to schedule a 1 hour technical interview with the CI/CD Lead
  * Senior and Staff candidates will then be invited to schedule a 2 hour product and technical discussion with the CI/CD Lead and Product Manager
* Next, candidates may be invited to schedule an interview with either an Engineer or Product Manager
* Candidates will be invited to schedule a interview with our VP of Engineering
* Finally, candidates may be invited to interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).
